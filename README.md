# Torrent Utils

Manage your Torrents in the qBittorrent client *easily*.

## Installation

Prerequisites:
- Python 3.10 or higher
- accessible qBittorrent WebUI

##### Credentials

Copy `.credentials.json.example` to `.credentials.json` and add your info

#### On Linux

1. `python -m venv .env`
2. `source .env/bin/activate`
3. `pip install -r requirements/_core.txt`
4. run program as described below

#### On Windows (Powershell)

1. `py -m venv .env`
2. `.env\Scripts\Activate.ps1`
3. `pip install -r requirements\_core.txt`
4. run program as described below


## Usage

The credentials to your qBittorrent Client will be read from your environment.
- `source .credentials.env`

### `index`

#### `create`

`python -m torrent_utils index create --file=index.json`

Creates and file with metadata about your torrents
that are available in your qBittorrent client

Additionally, you can add timestamps like the following (in the basename, but also if you want to create directories)

`%Y/%m-%c_index.json`

which will result in

`2023/01-23_index.json`

(documentation is [here](https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior))

#### `load`

`python -m torrent_utils index load --file=index.json`

Adds torrents from index.json to your qBittorrent client

### `magnet`

#### `create`

`python -m torrent_utils magnet create --directory=/media/movies`

Will create a file containing the magnet link to the torrent.
Also searches for Torrents in subdirectories

If `content_path` is a directory, magnet will be stored in it

Example:
If `content_path` is `/media/movies/american_psycho/` the Filepath will be:
`/media/movies/american_psycho/magnet`

But if `content_path` is `/media/movies/american_psycho.mp4` the Filepath will be:
`/media/movies/american_psycho.mp4.magnet`

#### `load`

`python -m torrent_utils magnet load --directory=/media/movies`

Will search subdirectories for magnet files, loads magnet files in similar way as described in *`create`*

#### `check`

`python -m torrent_utils magnet check --directory=/media/movies --verbose`

Checks if immediate Subdirectories (`/media/movies/*/`) if they contain a `magnet` file.
Will print out those directories, which do **not** contain a `magnet` file.

Option `--verbose` or `-v` print a message for every directory scanned.

### `subtitle`

#### `copy`

`python -m torrent_utils subtitle copy --directory=/media/movies/eternal_sunshine_of_the_spotless_mind/`

Will recursively search subdirectories for subtitle files, and copy them to where the video file is located.

Supported file extensions:

- `.sub`
- `.srt`
- `.vtt`
- `.ass`
- `.ssa`
- `.idx`

## `utils/`

### `list-magnets.sh`

lists the magnet files for immediate subdirectories

Prints the name of the directory and below the magnet link

## Cronjobs

I run my cronjobs on every 2nd day of the week at 00:00

The following line in my crontab generates index files

`0 0 * * */2 . /opt/torrent-utils/.credentials.env; PYTHONPATH=/opt/torrent-utils /opt/torrent-utils/.env/bin/python -m torrent_utils index create -f "/torrents/index/%Y/%m-%b/%d-index.json"`

And this line creates magnet files belonging to torrents

`0 0 * * */2 . /opt/torrent-utils/.credentials.env; PYTHONPATH=/opt/torrent-utils /opt/torrent-utils/.env/bin/python -m torrent_utils magnet create -d "/torrents"`
