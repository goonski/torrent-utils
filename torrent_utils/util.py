import json
import os
import sys

from loguru import logger

from torrent_utils import exceptions, PROJECT_ROOT
from torrent_utils.backend import Credentials


def load_credentials_from_file() -> Credentials:
    credentials_filepath = PROJECT_ROOT / '.credentials.json'
    try:
        raw_credentials = json.loads(
            open(credentials_filepath, mode='r', encoding='UTF-8').read()
        )

        credentials = Credentials(
            URL=raw_credentials['url'],
            username=raw_credentials['username'],
            password=raw_credentials['password'],
        )
    except FileNotFoundError as e:
        raise exceptions.NoCredentialsFileError(e)
    except KeyError as e:
        raise exceptions.NoCredentialsError(f'Could not find attribute {e} in .credentials.json file')

    return credentials


def get_credentials() -> Credentials:
    try:
        credentials = load_credentials_from_file()
        return credentials
    except Exception as e:
        logger.warning(e)
        sys.exit()
